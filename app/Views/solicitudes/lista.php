<?= $this->extend('layout/plantilla') ?>

<?= $this->section('content') ?>
<h6 style="">PABLO MOLERO</h6>
<div class="d-flex flex-row-reverse bd-highlight">
    <div class="p-2 bd-highlight">??</div>
    <?php if (session()->get('isLoggedIn') == FALSE): ?>
    <div class="p-2 bd-highlight"><button class="btn btn-success"><a href="http://localhost:8080/pablom/index.php/auth/logout" style="color: white;">salir</a></button></div>
    <?php else: ?>
    <div class="p-2 bd-highlight"><a href="http://localhost:8080/pablom/index.php/auth/login">entra</a></div>
    <?php endif; ?>
    <div class="p-2 bd-highlight"><button class="btn btn-dark"><i class="bi bi-cart-check-fill"> <a href="<?= site_url('cestaController') ?>" style="color:white;">&nbsp;Mira la cesta</a></i></button></div>
</div>
<a href="<?= site_url('pauController/afegir') ?>" class="btn btn-primary">Afegir</a>
<table class="table table-striped table-condensed" id="myTable">
    <thead>
        <th>NIE/NIF</th>
        <th>Solicitante</th>
        <th>email</th>
        <th>ciclo</th>
        <th>matrícula</th>
        <th></th>
    </thead>
    <?php foreach ($solicitudes as $solicitud): ?>
        <tr>
            <td><?= $solicitud['nif'] ?></td>
            <td><?= $solicitud['solicitante'] ?></td>
            <td><?= $solicitud['email'] ?></td>
            <td><?= $solicitud['nombre'] ?></td>
            <td>
                <?= $solicitud['tipo_tasa']==1 ? 'ordinaria' : ($solicitud['tipo_tasa']==3 ? 'gratuita' : 'semigratuita') ?>
            </td>
            <td><a href="<?= site_url('pauController/borrar/'.$solicitud['id'])?>" 
                   class="btn btn-danger btn-sm" onclick="return confirm('Estás seguro de borrar la solicitud de <?= $solicitud['solicitante'] ?>')">Borrar</a>
            <a href="<?= site_url('pauController/cistella/'.$solicitud['id'])?>" 
                   class="btn btn-warning btn-sm" onclick="return confirm('Vas a añadir a la cesta a <?= $solicitud['solicitante'] ?>')">Cistella</a></td>
            
        </tr>
    <?php endforeach; ?>
</table>

<?= $this->endSection() ?>