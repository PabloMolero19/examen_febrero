<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Models;

use CodeIgniter\Model;

/**
 * Description of pauModel
 *
 * @author jose
 */
class PauModel extends Model {
    protected $table      = 'pau';
    protected $primaryKey = 'id';
    protected $useAutoIncrement = true;

    protected $returnType     = 'array';
    protected $useSoftDeletes = true;
    protected $allowedFields = ['nombre', 'apellido1', 'apellido2', 'nif',
        'ciclo', 'email', 'tipo_tasa'];
    protected $deletedField  = 'deleted_at';
    protected $validationRules = [
        'nif'=>'required|is_unique[pau.nif]|nifValidation',
        'nombre' => 'required',
        'apellido1' => 'required',
        'email' => 'required|valid_email',
        'ciclo' => 'required|numeric',
        'tipo_tasa' => 'required|numeric',
    ];

}
